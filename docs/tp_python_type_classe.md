# 🐍 Types et classes

Les activités sur les tuple, list, dict sont à destination des enseignants uniquement. Ces activités sont du programme de NSI première.

## Tuple - tuple[^2] 
### :pencil2: exercice 1 : tuple

On considère le tuple `T = ("Barnabé", "Belfort", 17)`.

1. Que retourne l’instruction `T[1]`?
2. Quelle instruction permet d’obtenir la valeur 17?
3. Quel est l’état de la variable T après exécution de l’instruction suivante ? `T[1] = "Vesoul"`

### :computer: exercice 2 Géométrie
On se place dans un repère orthonormé $(O ; \overrightarrow{i}, \overrightarrow{j})$. On représente les coordonnées d’un point dans ce repère par un 2-uplet.

1. Écrire des tests et programmer la fonction milieu qui calcule les coordonnées du milieu d’un segment [AB] :
	``` py linenums="1"
	def milieu (A, B):
		""" 
		milieu (tuple , tuple ) -> tuple
	
		retourne les coordoonnées du milieu du segment [AB], 
			où A et B sont des tuples
			(on identifie le point avec ses coordonnées ) 
		"""
	```

2. Écrire des tests et programmer la fonction droite qui détermine une équation cartésienne de la droite (AB) :
	``` py linenums="1"
	def droite (A, B):
		""" 
		droite (tuple , tuple ) -> tuple
	
		retourne un tuple (a, b, c) correspondant aux coef 
			d’une équation cartésienne ax + by + c = 0 de la droite (AB)
		"""
	```

### :pencil2: exercice 3 Conversion binaire
On représente l’écriture binaire d’un entier naturel n codé sur 4 bits par un tuple, composé uniquement de 0 et de 1.

Écrire des tests **puis** programmer la fonction binToDec(T) permettant de déterminer la
valeur décimale d’un entier dont on connaît l’écriture binaire :
``` py linenums="1"
def binToDec (T):
	""" binToDec ( tuple ) -> int

		retourne l’entier écrit en base 10 dont 
		l’écriture binaire est donnée par le tuple T 
	"""
``` 
## Dictionnaire - dict[^2]

### :computer: exercice 4 Dictionnaires
On considère le p-uplet nommé  `T = {"nom": "Barnabé", "ville": "Belfort", "age": 17}` 

et s la chaîne de caractères définie par `s = "age"`.

Parmi les instructions suivantes, quelles sont celles qui permettent d’obtenir la valeur 17?

`T[2]` `T["2"]` `T[17]` `T["17"]` `T[age]` `T["age"]` `T[s]` `T["s"]`

### :computer: exercice 5 Carte d'identité

Sur le recto d’une carte d’identité apparaissent les éléments suivants :

![CNI](img/types_cni.jpg){width="300" align=left}

* Numéro (12 chiffres)
* Nom
* Prénom et parfois deuxième, troisième, ... prénoms
* Sexe (M ou F)
* Date de naissance
* Lieu de naissance
* Taille en mètres

---

1. Proposer une représentation de ces informations à l'aide des types de bases et des types construits. Préciser le type des valeurs associées à chaque descripteur.
2. Écrire la structure de données correspondant à sa propre carte d’identité à l’aide de cette représentation.
3. Les quatre premiers chiffres du numéro de la carte d’identité correspondent à l’année et au mois de délivrance de la carte d’identité.
4. Écrire des tests **puis** programmer une fonction estValable(carte) qui prend en argument une carte d’identité représentée par un dictionnaire et qui retourne True si la carte d’identité est valable à la date d’aujourd’hui, False si elle est périmée.
	``` py linenums="1"
	
	def estValable ( carte ):
		""" 
		estValable ( carte : dict ) -> bool
		Carte est un dictionnaire
			implémenté par un objet de type dict 
			correspondant à la description de la carte.
	
		Retourne True si la carte est valide
		Retourne False si elle est périmée 
		"""
	
	```

^^***Rappel***^^ *: la carte d’identité d’une personne majeur au moment de la délivrance de la carte est valable 15 ans et celle d’une personne mineur au moment de la délivrance de la carte d’identité est valable 10 ans*

## Les listes[^2] 
### :pencil2: exercice 6 Accès

On considère la liste tab défini de la façon suivante : `tab = [2,5,-8,12,7,3]`

1. Que vaut tab[1] ? tab[6] ? Quelle instruction permet d’obtenir le premier élément du tableau?
2. Après la suite d’instructions suivantes, que vaut tab ?
``` py linenums="1"
for i in range (6) :
	if tab [i ]%2 == 0:
	tab [i] = -tab [i]
```
3. Même question (en repartant du tableau initial), avec la suite d’instructions :
``` py linenums="1"
for i in range (6) :
	if i%2 == 0:
		tab [i] = -tab [i]
```

### :computer: exercice 7 Somme d'éléments

On veut écrire une fonction somme qui prend en argument un tableau d’entiers et qui retourne la somme des éléments du tableau. Programmer cette fonction.


### :computer: exercice 8 Renverser

On veut écrire une fonction renverse qui prend en argument un tableau d’entiers et qui retourne un nouveau tableau, dont les éléments sont les mêmes mais rangés dans l’ordre inverse.

1. Écrire le prototype de cette fonction.
2. Programmer cette fonction.

### :computer: exercice 9 Décaler
On considère la fonction décalage qui prend en argument un tableau d’entiers, et retourne un nouveau tableau obtenu par décalage de tous les éléments d’une place vers la droite (le dernier élément étant remis en première place).

1. Écrire le prototype de cette fonction.
2. Programmer cette fonction.
4. Modifier cette fonction pour qu’elle réponde aux spécifications suivantes :
``` py linenums="1"
def decalage (tab , k):
	""" 
	retourne un nouveau tableau obtenu par décalage de tous
	les éléments de k place vers la droite
	3 tab est un tableau d’entiers , k est un entier 
	"""
```

## Les classes 
### :computer: exercice 10 Applications élémentaires

``` mermaid
	classDiagram
		class Point {
			entier x
			entier y
			constructeur(val_x, val_y)
		}

		class Carre {
			Point p_a
			Point p_b
			constructeur(pa, pb)
			calculer_surface() : entier
		}

		class Verificateur {
			entier nb_calculs
			constructeur()
			calculer_distance_points(p1, p2): flottant
			test_point_dans_carre(point, carre):booleen
			remise_a_zero()
		}

		Carre "1"*-->"2" Point
		Verificateur ..> Point
		Verificateur ..> Carre

```

La classe vérificateur possède un attribut `nb_calculs` incrémentée à chaque calcul et qu'il est possible de remettre à zéro par la méthode `remise_a_zero()`

1. écrire les classes `Point`, `Carre` et `Verificateur` en python
2. écrire un programme qui crée les points pa, pb, pc, pd, pe, pf dont vous choisirez les coordonnées
3. créer 2 carrés à partir de points que vous avez créés précédemment
4. tester la présence des 2 points restants dans les 2 carrés en appelant la méthode `test_point_dans_carre()`
5. compléter en affichant la distance entre 2 points (vérifier la valeur par le calcul) 
6. afficher 2 

!!! notes
	Pour faire x^2^, on peut faire `x**2`
	Pour faire la racine carré, on peut utiliser la fonction sqrt de la bibliotèque math.
	Pour cela il convient de l'importer en début de programme :
	- `from math import sqrt` : dans ce cas l'appel aura la forme `sqrt(25)`
	- `import math` : dans ce cas l'appel aura la forme `math.sqrt(25)`. Dans ce cas, on retrouve la notation objet ; conceptuellement, on appelle la méthode `sqrt()` de l'objet `math`.

!!! notes
	utiliser la fonction print("Ma variable", var_a) pour afficher les valeur de retour des méthodes.


### :pencil: exercice 11 penser objet

Prenez comme support votre cuisine, identifier des objets (table, chaise, vaisselle, lave-vaisselle ...) et tentez l'élaboration d'un diagramme de classe.

[^2]: largement inspirer par le travail de Patrice Perrot.
