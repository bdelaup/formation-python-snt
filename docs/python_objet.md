# Python et l'objet

## Introduction

Considérons deux aspects des traitements informatiques:

* l'information : c'est ce qui caractérise l'état de la machine 
* les traitements : les traitements vont permettre de passer d'un état initial à un état final

```mermaid
graph LR
  A([Etat A])
  B([Etat B])

  A -- "Traitements" --> B
```

## La programmation impérative

On connaît les variables qui symbolisent des informations.
On connaît également l'opérateur d'affectation qui permet de modifier l'état d'une variable en fonction de :

* Une ou plusieurs variable
* Un opérateur ou une fonction

Une variable se définit par :

- son type
- son nom

``` py linenums="1"
a = 10
b = 9
c = a + b
# c symbolise le contenu d'un espace mémoire contenant la valeur 19
```

![Etapes](img/etat_imperatif.drawio.png)

!!! definition "Programmation impérative"
    L'état de la machine est caractérisée par :

    - le contenu des variables 
    - l'instruction en cours de traitement (pointeur d'instruction)

    Les traitements sont :

    - les instructions en cela que le passage d'une instruction à l'autre modifie le pointeur d'instruction
    - l'opérateur d'affectation




## La programmation orientée objet

Selon Wikipedia[^poo] la programmation orientée objet consiste en la définition et l'interaction de briques logicielles appelées objets.

Un objet :
- représente un concept, une idée ou toute entité du monde physique
- possède une structure interne (ses attributs) et un comportement (ses méthodes) 
- sait interagir avec ses pairs. 

Concevoir objet revient à représenter ces objets et leurs relations.

La programmation par objet peut être élaborée en utilisant des méthodologies de développement logiciel objet, dont le *Unified Modeling Language* (UML, pré-curseur et sous ensemble du SysML). 

!!! definition "Programmation orienté objet"
    L'état de la machine est caractérisée par :

    - les relations entre les objets
    - l'état des attributs des objets

    Les traitements sont :

    - Les méthodes permettant d'accéder aux attributs d'un objet.

En programmation orienté objet 

- les types d'objet sont appelés des classes.
- les attributs sont propre à chaque objet
- les méthodes sont partagées par tous les objets d'une même classe
- une classe défini la façon dont les attribut sont initialisés à la création de l'objet.


!!! definition "Classe Vs. Objet"

	- La classe est le type, l'objet est la variable
	- La classe est le moule, l'objet est le gâteau
	- La classe est le concept, l'objet est le concret

	- On instancie une classe == on crée un objet à partir de la classe
	- Instancier == matérialiser 

!!! exemple "Modéliser des formes géométriques"
    ``` mermaid
    classDiagram
      class segment {
		entier x_a
		entier y_a
		entier x_b
		entier y_b		
		constructeur(x_a, y_a, x_b, y_b)
		calculer_longueur()
		agrandir(coefficient)        
      }
    ```

    - Le nom de la classe est indiqué en haut.
    - Les attribut sont en-dessous
    - Les méthodes sont identifiables par les parenthèses

	Les classes ont une méthode particulière nommée constructeur. Cette méthode est utilisée pour créer des instance de la classe, c'est à dire un objet de cette classe.

Dans un programme il conviendra aussi de créer des relation entre les objets.

!!! exemple Modéliser des formes géométriques
	

	``` mermaid
	classDiagram
		class segment {
			entier x_a
			entier y_a
			entier x_b
			entier y_b	
			constructeur(x_a, y_a, x_b, y_b)
			calculer_longueur()
			agrandir(coefficient)        
		}

		class triangle {

			segment segment_1
			segment segment_1
			segment segment_1
			constructeur(segment_1, segment_2, segment_3)
			calculer_périmètre()
		}

		triangle "1"*-->"3" segment
	```
	On dit alors que les  objets de classe triangle sont composés de 3 segments

!!! definition
	on appelle encapsulation le fait d'associer des attributs et des méthodes à un objet

## L'objet en python'

Voici la façon d'écrire l'exemple précédent en python.

Tout d'abord, il convient de décrire les classes

``` py title="Classes" linenums="1"
from math import sqrt # dans math, utiliser sqrt
class Segment:
	def __init__(self, x_a, y_a, x_b, y_b):
		self.xa = x_a
		self.ya = y_a
		self.xb = x_b
		self.yb = y_b
	
	def calculer_longueur(self):
		longueur = sqrt((self.xb-self.xa)**2 + (self.yb-self.ya)**2)
		return longueur

class Triangle:
	def __init__(self, seg1, seg2, seg3):
		self.seg1 = seg1
		self.seg2 = seg2
		self.seg3 = seg3
	def calculer_perimetre(self):
		perimetre = self.seg1.calculer_longueur()\
			+self.seg2.calculer_longueur()\
			+self.seg3.calculer_longueur()
		return perimetre
```
Noter la présence du `self`. Lorsqu'une classe est instancier sous la forme d'un objet

Ensuite il faut écrire un programme qui va instancier les objets est les utiliser.

``` py title="main.py" linenums="1"

# Création des segments
segment1 = Segment(0,0,2,1)
segment2 = Segment(2,1,4,-1)
segment2 = Segment(4,-1, 0, 0)

# Création du triangle
triangle = Triangle(segment1, segment2, segment3)

# Affichons le périmètre du triangle dans la console
permimetre = triangle.calculer_perimetre()
print(perimetre)

```

## L'héritage

L'héritage consiste à créer un lien de filiation entre les classes. Ainsi si B hérite de A, alors il B possède tous les attributs et méthode de A.

On dit que A est la généralisation de B, et B la spécialisation de A.

!!! exemple 
	``` mermaid
	classDiagram
		class segment {
			entier x_a
			entier y_a
			entier x_b
			entier y_b	
			constructeur(x_a, y_a, x_b, y_b)
			calculer_longueur()
			agrandir(coefficient)        
		}

		class triangle {
			constructeur(segment_1, segment_2, segment_3)
		}
		
		class carré {
			constructeur(segment_1, segment_2, segment_3, segment_4)
		}


		class polygone {

			segment []: liste_segments
			calculer_périmètre()
		}

		polygone "1"*-->"*" segment
		polygone <|-- triangle
		polygone <|-- carré

	```
	La flèche avec une tête triangulaire matérialise la généralisation.
	
	- Le triangle est un polygone particulier
	- Le triangle est un polygone spécialisé
	- Le triangle hérite du polygone
	- Le polygone est une généralisation du triangle

``` py title="heritage" linenums="1"
class polygone :
	def __init__(self):
		self.liste_segments=[] # liste de coté
		self.couleur="noire"

	def calculer_perimetre(self):
		perimetre = 0

		for cote in self.liste_segments:
			perimetre = perimetre + cote.calculer_longueur()

		return perimetre
	
	def changer_couleur(self, clr):
		self.couleur=clr

class triangle (polygone):
	def __init__(self, seg1, seg2, seg3):
		self.liste_segments.append(seg1)
		self.liste_segments.append(seg2)
		self.liste_segments.append(seg3)
```

Dans cet exemple, triangle hérite des méthode `calculer_perimetre()`, `changer_couleur()` et des attributs `liste_segments`, `couleur`

Le code ci-dessous est donc possible :

``` py linenums="1"
# Création des segments
segment1 = Segment(0,0,2,1)
segment2 = Segment(2,1,4,-1)
segment2 = Segment(4,-1, 0, 0)

# Création du triangle
triangle = Triangle(segment1, segment2, segment3)

# Affichons la couleur
print(triangle.couleur)

```

## La notion de polymorphisme

Le polymorphisme est le troisième pilier de la programmation orientée objet.

L'idée est de programmer des traitements sur des classes parente. 

### L'exemple de la pizzeria

Imaginons une pizzeria qui aurait pour mission de fabriquer des pizzas, les mettre dans un cartons et les livrer:

- Les pizzas peuvent être faites par un seul et même cuisinier
- Il n'existe qu'un format de carton
- N'importe quel véhicule peut être utiliser pour livrer n'importe quelle pizza

On n'aurait pas l'idée de spécialiser un cuisinier pour un type de pizza ou d'utiliser un carton par type de pizza.

On préfère considérer qu'une pizza au fromage est une sorte particulière de pizza, qu'une pizza au jambon est une sorte particulière de pizza... on dirait alors que ces pizza "spécialisée" héritent des propriétés des pizzas en général comme par exemple le diamètre ou l'épaisseur. Par contre chaque pizza spécialisé possédera en attribut une liste d'ingrédient et une recette différente ainsi qu'une méthode de fabrication différente.

Ce faisant, on peut alors que considérer que tout cuisinier sachant préparer et emballer une pizza saura le faire avec n'importe que type de pizza.

 Il ne sera pas nécessaire d'avoir un cuisinier par type de pizza du coup moins de charge de personnel pour le restaurateur. Ce qui se traduit en informatique, pour raccourcir, moins de code, moins de bug, moins de tests ...

``` mermaid
classDiagram
	class Pizza {
		diamètre = 33
		épaisseur = 4.2
		ne_pas_couler_dans_la_boite()
	}

	class PizzaFromage {
		ingredients = [pate, tomate, fromage]
		recette = "..."
	}
	
	class PizzaJambon{
		ingredients = [pate, tomate, jambon]
		recette = "..."
	}


	class cuisinier {		
		préparer_pizza() Pizza
		emballer_pizza(pizza) CartonDePizza
	}

	CartonDePizza o--> Pizza
	cuisinier ..> Pizza
	cuisinier ..> CartonDePizza
	Pizza <|-- PizzaFromage
	Pizza <|-- PizzaJambon

```
La flèche avec une tête losange matérialise l'agrégation, c'est à dire l'assemblage.
La flèche pointillés matérialise l'utilisation.

### En géométrie et en python

Imaginons que l'on souhaite ajouter des formes géométriques à un canevas. Ce canevas devra pouvoir nous indiquer la surface occupée par les formes géométriques.

!!! exemple 
	``` mermaid
	classDiagram
		class segment {
			calculer_longueur()
			agrandir(coefficient)        
		}

		class triangle {
			constructeur(segment_1, segment_2, segment_3)
			calculer_aire()	
		}
		
		class carré {
			constructeur(segment_1, segment_2, segment_3, segment_4)
			calculer_aire()	
		}

		class polygone {
			segment []: liste_segments
			calculer_périmètre()
			*calculer_aire()*
		}

		class canevas {
			liste_formes [] : polygone
			calculer_aire_recouverte()
			calculer_somme_perimetre()
		}
		
		polygone "1"*-->"*" segment
		polygone <|-- triangle
		polygone <|-- carré

		canevas "1"o-->"*" polygone

	```

Noter que la classe polygone possède une méthode `calculer_aire()`	. Or un polygone est en mesure de calculer sa surface mais pas son aire qui est spécifique à la forme géométrique.

La méthode `calculer_aire()` de la classe polygone est dite virtuelle : elle n'existe pas vraiment mais toutes les classe qui héritent de la classe polynôme implémentent la méthode `calculer_aire()`. Ce sera donc l'implémentation des classe fille se substituera à celle de la classe mère lors d'un appel.

Et en python :

``` py title="Polymorphisme" linenums="1"
class Polygone :
	def __init__(self):
		self.liste_segments=[] # liste de coté
		self.couleur="noire"

	def calculer_perimetre(self):
		perimetre = 0

		for cote in self.liste_segments:
			perimetre = perimetre + cote.calculer_longueur()

		return perimetre
	
	def changer_couleur(self, clr):
		self.couleur=clr

	def calculer_aire(self):
		raise NotImplementedException()

class Triangle (polygone):
	def __init__(self, seg1, seg2, seg3):
		self.liste_segments.append(seg1)
		self.liste_segments.append(seg2)
		self.liste_segments.append(seg3)
	
	
	def calculer_aire(self):
		aire = ... # base * hauteur / 2
		return aire

class Carre (polygone):
	def __init__(self, seg1, seg2, seg3, seg4):
		self.liste_segments.append(seg1)
		self.liste_segments.append(seg2)
		self.liste_segments.append(seg3)
		self.liste_segments.append(seg4)

	def calculer_aire(self):
		aire = ... # longueur * longueur
		return aire

class Canvas:
	def __init__(self):
		self.liste_polygones = []
	
	def surface_recouverte(self):
		s = 0
		for poly in self.liste_polygones:
			s = poly.calculer_aire()
		return s

	def perimetre_total(self):
		p = 0
		for poly in self.liste_polygones:
			p = poly.calculer_perimetre()
		return p

	def ajouter_forme(self, forme):
		self.liste_polygone.append(forme)
```

Dans cet exemple, triangle hérite des méthode `calculer_perimetre()`, `changer_couleur()` et des attributs `liste_segments`, `couleur`

Le code ci-dessous est donc possible :

``` py linenums="1"
# Création des segments
segment1 = Segment(0,0,2,1)
segment2 = ...
...
segmentn = ...

# Création des formes
triangle1 = Triangle(segment1, segment2, segment3)
triangle2 = Triangle(segment13, segment21, segment40)
carre1 = Carre(segment13, segment21, segment40, segment25)

# Création du canevas
mon_canevas = Canvas

mon_canevas.ajouter_forme(triangle1)
mon_canevas.ajouter_forme(triangle2)
mon_canevas.ajouter_forme(carre1)

# Affichage du
aire = mon_canevas.surface_recouverte()
perimetre = mon_canevas.calculer_perimetre()

print("Aire", aire, "Périmètre", perimetre)

```
Lors de l'appelle `mon_canevas.surface_recouverte()`, c'est bien les méthode `calculer_aire()` des `Carre` et `Triangle` qui seront appelés et non celle de la classe `Polygone`. 

Par contre, lors de l'appel de `mon_canevas.calculer_perimetre()` ce sera la méthode `calculer_perimetre()` de la classe Polygone qui sera appelé puisqu'elle n'est pas redéfinie dans les classe triangle et carré.

## Python et l'objet finalement

En python tout est objets : modules, fichier, fonction... 

Même les classes sont des objets. De quoi méditer encore un moment sur le sujet !

[^poo]: [Programmation orientée objet sur wikipedia](https://fr.wikipedia.org/wiki/Programmation_orient%C3%A9e_objet)