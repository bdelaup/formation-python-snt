# ✔️ Types et classes - Correction

Les activités sur les tuple, list, dict sont à destination des enseignants uniquement. Ces activités sont du programme de NSI première.

## Tuple - tuple[^2] 
### :pencil2: exercice 1 : tuple - correction

``` py
T = ("Barnabé", "Belfort", 17)
print(T[1])

T[2] = Vesoul

print(T) # T = ("Barnabé", "Vesoul", 17)
```

==>

```sh
Belfort
Traceback (most recent call last):
  File "C:\Users\benoit\lycee\OneDrive - LYCEE Jules Haag\formation_python\formation-python-snt\docs\assets\python\type-exo1.py", line 4, in <module>
    T[2] = "Vesoul"
TypeError: 'tuple' object does not support item assignment

```

Car un tuple, une fois créé, ne peut pas être modifier... il conviendrait d'utiliser une list.

### :computer: exercice 2 Géométrie - correction
On se place dans un repère orthonormé $(O ; \overrightarrow{i}, \overrightarrow{j})$. On représente les coordonnées d’un point dans ce repère par un 2-uplet.

1. Écrire des tests et programmer la fonction milieu qui calcule les coordonnées du milieu d’un segment [AB] :
	``` py linenums="1"
	def milieu (A, B):
		""" 
		milieu (tuple , tuple ) -> tuple
	
		retourne les coordoonnées du milieu du segment [AB], 
			où A et B sont des tuples
			(on identifie le point avec ses coordonnées ) 
		"""
	```

### :pencil2: exercice 3 Conversion binaire - correction

## Dictionnaire - dict[^2]

### :computer: exercice 4 Dictionnaires
On considère le p-uplet nommé  `T = {"nom": "Barnabé", "ville": "Belfort", "age": 17}` 

et s la chaîne de caractères définie par `s = "age"`.

`T["17"]` `T["age"]` `T[s]`

### :computer: exercice 5 Carte d'identité

Sur le recto d’une carte d’identité apparaissent les éléments suivants :

![CNI](img/types_cni.jpg){width="200" align=right}

* Numéro (12 chiffres)
* Nom
* Prénom et parfois deuxième, troisième, ... prénoms
* Sexe (M ou F)
* Date de naissance
* Lieu de naissance
* Taille en mètres

---
``` py linenus="1"
carte = {
	'numero' = "123546789123",
	'nom' = "Dupont",
	'prenoms' = ["Dupond","walker"],
	'sexe' = 'F',
	'date_de_naissance' = [10,12,2023],
	'lieux' = "mars",
	'taille' = 1.8
}
```

## Les listes[^2] 
### :pencil2: exercice 6 Accès

On considère la liste tab défini de la façon suivante : `tab = [2,5,-8,12,7,3]`

1. Que vaut tab[1]->5, tab[6]->Erreur index hors du tableau.
2. Après la suite d’instructions suivantes, que vaut tab ?
``` py linenums="1"
for i in range (6) :
	if tab [i ]%2 == 0:
	tab [i] = -tab [i]
```
3. Même question (en repartant du tableau initial), avec la suite d’instructions :
``` py linenums="1"
for i in range (6) :
	if i%2 == 0:
		tab [i] = -tab [i]
```

### :computer: exercice 7 Somme d'éléments

``` py linenums="1"
def somme(tab):
	taille = len(tab)
	s = 0
	for i in range(0, taille):
		s = s + tab[i]

	return s
```

### :computer: exercice 8 Renverser

On veut écrire une fonction renverse qui prend en argument un tableau d’entiers et qui retourne un nouveau tableau, dont les éléments sont les mêmes mais rangés dans l’ordre inverse.

1. Écrire le prototype de cette fonction.
2. Programmer cette fonction.

``` py linenums="1"
def inverse(tab_entree):
	taille = len(tab_entree)
	tab_sortie = [0]*taille

	for i in range(0, taille):
		tab_sotie[i] = entrer[taille -i -1]

	return tab_sortie
```

### :computer: exercice 9 Décaler
On considère la fonction décalage qui prend en argument un tableau d’entiers, et retourne un nouveau tableau obtenu par décalage de tous les éléments d’une place vers la droite (le dernier élément étant remis en première place).

1. Écrire le prototype de cette fonction.
2. Programmer cette fonction.
4. Modifier cette fonction pour qu’elle réponde aux spécifications suivantes :
``` py linenums="1"
def decalage (tab , k):
	taille = len(tab)

	dernier = tab[taille-1]

	for i in range(0, taille-1):
		tab[taille -i ] = tab[taille -i -1]

	tab[0]= dernier

	return tab
```

## Les classes 
### :computer: exercice 10 Applications élémentaires

``` mermaid
	classDiagram
		class Point {
			entier x
			entier y
			constructeur(val_x, val_y)
		}

		class Carre {
			Point p_a
			Point p_b
			constructeur(pa, pb)
			calculer_surface() : entier
		}

		class Verificateur {
			entier nb_calculs
			constructeur()
			calculer_distance_points(p1, p2): flottant
			test_point_dans_carre(point, carre):booleen
			remise_a_zero()
		}

		Carre "1"*-->"2" Point
		Verificateur ..> Point
		Verificateur ..> Carre

```
``` py
class Point:
	def __init__(self, x, y):
		self.x = x
		self.y = y

class Carre:
	def __init__(self, p_a, p_b):
		self.p_a = p_a
		self.p_b = p_b
	
	def calculer_surface(self):
		surface = (self.p_b.x + self.p_a.x) * (self.p_b.y + self.p_a.y)

```

### :pencil: exercice 11 penser objet

Prenez comme support votre cuisine, identifier des objets (table, chaise, vaisselle, lave-vaisselle ...) et tentez l'élaboration d'un diagramme de classe.

``` mermaid
	classDiagram
		class Pied{
			constructeur(xA, yA, xB, yB)
		}


		class TableCarre {
			constructeur(pied1, pied2, pied3, pied4)
		}

		TableCarre "1"*-->"2" Pied

```

[^2]: largement inspirer par le travail de Patrice Perrot.
