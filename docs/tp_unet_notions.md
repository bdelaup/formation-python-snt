# 🕮 Notions
## Objectifs d'apprentissage

L'objectif de ce tp est de faire toucher du doigt le fonctionnement des réseaux au des objets connecté au travers d'une activité dont l'objet et de créer un réseau de carte microbit qui publiera des informations de capteur sur microbit. Cette expérience sera prolongée par la récupération de l'information sur un téléphone mobile.


## La pile internet

La pile protocolaire retenue par les programme de SNT est dite la *pile internet*.

C'est un modèle en quatre couche ayant chacun une fonction spécifique:

![desctiption pile internet](img/unet_pile_internet_desc.png)

![Pile internet](img/unet_pile_internet.png)

## Topologie

| #           | Réseau en étoile                                             | Réseau maillé                                                |
| ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Type        | ![Etoile](img/unet_reseau_topo_etoile.drawio.png){width=150} | ![Maillé](img/unet_reseau-topo-maille.drawio.png){width=150} |
| Application | Réseau local  (lycée, réseau domestique)                     | Internet                                                     |

## Exemple : réseau domestique  

![Exemple réseau](img/unet_reseau-domestique.drawio.png)

| #  | Description                                            |
| -- | ------------------------------------------------------ |
| 1  | Réseau local domestique                                |
| 2  | Réseau global                                          |
| 3  | Réseau local d'entreprise                              |
| 4  | Switch (commutateur)                                   |
| 5  | Routeur dit de bordure                                 |
| 6  | Routeur du fournisseur d'accès internet                |
| 7  | Machines du réseau domestique                          |
| 8  | Machines du réseau administratif de l'entreprise       |
| 9  | Serveur web de l'entreprise                            |
| 10 | Liaison physique (cable ethernet, wifi, fibre, 4G ...) |

## DNS 

Il est souvent compliquer de retenir une adresse IP d'autant plus que celle-ci changent parfois (on dit que l'IP est flottante).

Aussi, on associe les adresse IP à des nom de domaine.

Par exemple wikipedia.org est associé à `198.35.26.96`. Il est plus facile de retenir wikipedia.org que son adresse IP.

Prenons l'exemple de la visite d'une page wikipedia à l'aide d'un navigateur.
1. vous saisissez `wikipedia.org` dans la barre d'adresse.
2. le navigateur interroge un annuaire appelé serveur D.N.S. pour connaître l'adresse IP associée.
3. à l'aide de l'adresse IP obtenue, le serveur interroge le serveur de wikipedia pour obtenir la page web associée.

!!! definition "DNS"
	**D.N.S.** : Domain Name System. En français, Système de nom de domaine.
	
	Les serveurs DNS maintiennent à jour un annuaire faisant correspondre une adresse IP à chaque nom de domaine. 

	Exemple : `wikipedia.org` --> `198.35.26.96`

	L'adresse du serveur DNS que l'ordinateur doit utilisé est généralement fournie au système d'exploitation par le fournisseur d'accès à internet.

![DNS](img/unet_dns.png){width=600} 

## Le protocole MQTT

Le protocole MQTT est au sommet de la pile internet, c'est un protocole d'application.

Il est très répandu dans le monde de l'internet des objets. Il fonctionne sur un principe d'abonnement/notifications et de publication.

Les noeuds connectés au réseau MQTT peuvent :

* Un utilisateur s'abonne à un sujet appelé `topic`
* Un autre utilisateur publie une information sur le `topic`
* Tous les abonnées reçoivent une notification

![mqtt archi](img/unet_mqtt.png)

Les caractéristiques suivantes en font un protocole populaire:

- le protocole ne consomme pas beaucoup de données
- permet de configurer des niveaux de services
    - QoS 1 : un message transmis tout au plus : *message*
    - QoS 2 : un message transmis au moins : *message -> acquittement* 
    - QoS 3 : un message transmis exactement : *message + acquittement + acquittement de l'acquittement*
- il fonctionne au-dessus d'une "socket" TCP bi-directionnelle et ne nécessite pas de *polling* sur le serveur.
- il est possible d'utiliser un mode de persistance des données sur le serveur pour qu'un abonné hors ligne puisse récupérer les publication (ou `post`) plus tard.

![mqtt sequence](img/unet_mqtt_sequence.png)

De nombreuses ressources sont disponibles ici[^hivemq].

Voici quelque broker public et gratuit possible sur TCP:1883:

- `mqtt.eclipseprojects.io`
- `broker.hivemq.com`
- `test.mosquitto.org`

:warning: Ce sont des serveurs de test. Les éditeurs s'en serve pour tester leur produit qui seront ensuite proposés en version payante.
L'état du serveur n'est donc pas garanti. La rétention de données dans le temps est au bon vouloir de l'éditeur. Bref, ça va pour du test.

[^hivemq]: [https://www.hivemq.com/mqtt/mqtt-protocol/](https://www.hivemq.com/mqtt/mqtt-protocol/)



