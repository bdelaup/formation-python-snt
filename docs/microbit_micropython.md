# Microbit Micropython

## Documentation
Le site officiel de la documentation de micropython pour microbit est disponible ici  :
[https://microbit-micropython.readthedocs.io/fr/latest/](https://microbit-micropython.readthedocs.io/fr/latest/)


## Programme principale

En python un fichier peut-être importé comme n'importe quel autre module. 

Par exemple : 

``` py linenums="1" title="mon_module.py" hl_lines="12 13"
class MaClasse:
    def ma_methode1(self):
        print("ma_methode1")
    def ma_methode2(self):
        print("ma_methode2")

print("bonjour")

def ma_fonction(self):
    print("ma_fonction")

if __name__=="__main__":
    ma_fonction()
```

Pour que le contenu de ce fichier (`mon_module.py`) soit exécuté en tant que programme principal:
- il faut faire `import mon_module` depuis la console (REPL)
- le contenue du fichier est dans `main.py`

Ici, le programme afficherait:
```sh
> python mon_module.py
ma_fonction
```

Considérons le script `mon_script_v1.py` :
``` py linenums="1" title="mon_script_v1.py" hl_lines="1"
from mon_module import MaClasse, ma_fonction

mon_objet = MaClasse()

mon_objet.ma_methode1()
mon_objet.ma_methode2()

ma_fonction()
```

``` sh
> python mon_script_v1.py
ma_methode1
ma_methode2
ma_fonction 
```
On aurait pu importé toutes les objets de `mon_module.py` l'inconvénient est l'utilisation de la mémoire: 
``` py linenums="1" title="mon_script_v2.py" hl_lines="1"
from mon_module import *
```

Alternativement, il est possible d'importé le module dans sa globalité. 
``` py linenums="1" title="mon_script_v1.py" hl_lines="1 3 8"
import mon_module

mon_objet = mon_module.MaClasse()

mon_objet.ma_methode1()
mon_objet.ma_methode2()

mon_module.ma_fonction()
```

Affichera :
``` sh
> python mon_script_v1.py hl_lines="1"
bonjour
ma_methode1
ma_methode2
ma_fonction 
```

Noté que l'appel de `print("bonjour")` est alors exécuté au moment de l'import.


## Journalisation

Il existe une fonction de journalisation qui permet de récupérer d'enregistrer simplement des données.

``` py linenums="1" title="Exemple de journalisation" hl_lines="9 11 12 15"
from microbit import accelerometer, button_a, button_b
from microbit import display, Image
import log, time

while not button_a.was_pressed():
        pass

if button_b.was_pressed():
    log.delete(True)
    
log.set_labels('x', 'y', 'z')
log.set_mirroring(True)

while not button_a.was_pressed():
    log.add({
      'x': accelerometer.get_x(),
      'y': accelerometer.get_y(),
      'z': accelerometer.get_z()
    })
    
    time.sleep(0.1)

display.show(Image.YES)
```

Pour mettre en oeuvre :

1. Téléverser le programme
2. Presser B : réinitialisation du fichier de log
3. Presser A : démarrage de l'enregistrement
5. Presser A : fin de l'enregistrement
6. Débrancher et rebrancher la carte microbit
7. Ouvrir le fichier `MY_DATA.HTM` à la racine du système de fichier de la carte microbit
8. Visualiser les données sous forme de tableau
9. Visualiser les données sous forme de graphe

![fs](img/mb_fs.png){width=150}
![fs](img/mb_log_data.png){width=250}
![fs](img/mb_log_graph.png){width=350}

Vous pouvez également exporter les données au format csv (séparateur : `,`)

Sous Excel, il faut ensuite les convertir : données -> outils de données -> convertir

## Timer

Avec micro:bit V2 il est possible d'exécuter une fonction périodiquement.

``` py linenums="1" title="Utilisation d'un timer" hl_lines="6"

from microbit import run_every, temperature, microphone, display
import log

log.set_labels('temperature', 'sound', 'light')

@run_every(s=30)
def log_data():
    log.add({
      'temperature': temperature(),
      'sound': microphone.sound_level(),
      'light': display.read_light_level()
    })

```
## Détails sur le module Radio

* 84 `channel` - fréquences (possible)
* file d'attente entrante de 3 messages (configurable)
* 1 adresse physique (fixe)
* 255 adresses logique
* débit 255 Kbit à 2 Mbit

Voir la documentation [https://microbit-micropython.readthedocs.io/fr/latest/radio.html](https://microbit-micropython.readthedocs.io/fr/latest/radio.html)

