# 🐍 Activités

## Mise en situation

Nous disposons d'un fichier au format CSV contenant les 36000 commune de France dont les descripteurs sont les suivants :

``` csv title="Descripteurs de villes_france.csv"
departement	
nom	code_poste	
population	
surface	
longitude	
latitude	
altitude_min	
altitude_max	
nom_cpt_temperature	
nom_cpt_lumiere
```

Par ailleurs, nous disposons du relevé de température de plusieurs capteur :
[releve_temperature_avril_2023](https://bdelaup.gitlab.io/formation-python-snt/assets/pandas/releve_temperature_avril_2023.csv)
``` csv title="Descripteurs de releve_temperature_avril_2023.csv"
date nom_capteur temperature
```
L'objectif de l'activité est de :

1. déterminer la température moyenne et maximum de chaque capteur.
2. déterminer ville et les coordonnées GPS de chaque capteur


## Mise en action

### :pencil2: Tableau à compléter

Tableau à compléter tout au long de l'activité

| Nom du capteur | Temperature moyenne | Temperature maximum | Ville | Longitude | Latitude |
| -------------- | ------------------- | ------------------- | ----- | --------- | -------- |
| -              |                     |                     |       |           |          |
| -              |                     |                     |       |           |          |
| -              |                     |                     |       |           |          |
| -              |                     |                     |       |           |          |
| -              |                     |                     |       |           |          |
| -              |                     |                     |       |           |          |


### :computer: Exo 1 - déterminer les valeurs moyennes et max de chaque capteur

1. Charger le fichier `releve_temperature_avril_2023.csv` dans la variable `temperatures`
``` py linenums="1"
from pandas import read_csv
fichier_temp = "releve_temperature_avril_2023.csv"
temperatures = read_csv(fichier_temp, sep=";")
print(temperatures.columns)
```
2. exécuter la commande `temperatures.groupby(by="nom_capteur").groups.keys()` et noter le nom des capteurs
``` py linenums="1"
groupes_capteur = temperatures.groupby(by="nom_capteur").groups.keys()
print(groupes_capteur) 
```
3. Pour chaque capteur récupérer la température maximum et la noter, exemple:
``` py 
print("tmp1571 :", temperatures.loc[temperatures["nom_capteur"] == "tmp1571"].temperature.max())
```
4. Pour chaque capteur récupérer la température moyenne et la noter `mean(numeric_only=True)` à la place de `max()`

### :computer: Exo 2 - Localisation
Il s'agit d'appliquer les mêmes démarche que dans l'exercice 1

1. Charger le fichier `villes_france.csv` dans la variable `villes`
2. Pour chaque capteur (tmp1571, tmp18972, tmp23730, tmp30548, tmp31739, tmp803), récupérer le nom de la ville où il se situe et le noter.
``` py linenums="1"
# Nom de villes
capteurs = ["tmp1571", "tmp18972", "tmp23730", "tmp30548", "tmp31739", "tmp803"]
for capteur in capteurs:
    villes_capteur = villes.loc[(villes["nom_cpt_temperature"]== capteur),["nom","nom_cpt_temperature"]]
    print (villes_capteur, "\n")
```
3. Pour chaque capteur, récupérer les coordonnées GPS et les noter































