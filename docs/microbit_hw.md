# Le hardware micro:bit
## Histoire


![BBC_Micro_Front_Restored](img/BBC_Micro_Front_Restored.jpg){width=120 align=right }

En 1981, le BBC Microcomputer System, ou BBC Micro, est une série de micro-ordinateurs construits par Acorn Computers Ltd pour le BBC Computer Literacy Project initié par la British Broadcasting Corporation. Créés pour une utilisation pédagogique, les ordinateurs de la gamme BBC Micro sont réputés pour leur modularité et la qualité de leurs systèmes d'exploitation.[^bbc_micro] 

![BBC micro:bit](img/microbit-front.png){width=150 align=right }


Lancé en juillet 2015 par la BBC, le projet microbit prévoit de distribuer gratuitement un million d'exemplaires à des écoliers britanniques de onze et douze ans (la septième année ou l'équivalent) pour leur apprendre les fondements de la programmation. Le but est à la fois de familiariser les enseignants avec ces technologies, mais aussi d’initier les enfants avec des cas simples et concrets.

![BBC micro:bit](img/microbit-back.png){width=150 align=right}

La BBC déclare : « Le BBC micro:bit est un ordinateur de poche que vous pouvez programmer, personnaliser et contrôler afin de rendre concrets vos idées numériques, des jeux et des applications. » Il a été pensé afin de pousser les plus jeunes à découvrir facilement les « joies » de la programmation. Une initiative qui s'inscrit dans la lignée des annonces des autorités qui ont ajouté l'éveil au code pour les élèves dès l'école primaire.[^bbc_microbit]


## Description matériel [^hardware]

!!! warning Commentaire sur les versions
	Dans ce qui, sauf mention contraire, les informations fournies sont relative à la version 2.2 de la carte micro:bit.


![Microbit description](img/microbit-overview-2-2.png){width=500}


### Processeur applicatif *Architecture Arm*

| version      | V2,2                                    | v1,5                           |
| ------------ | --------------------------------------- | ------------------------------ |
| Model        | Nordic nRF52833                         | Nordic nRF51822-QFAA-R rev 3   |
| Core variant | Arm Cortex-M4 32 bit processor with FPU | Arm Cortex-M0 32 bit processor |
| Flash ROM    | 512KB                                   | 256KB                          |
| RAM          | 128KB                                   | 16KB                           |
| Speed        | 64MHz                                   | 16MHz                          |


### Radio		

| Protocol           | Micro:bit Radio                  | Micro:bit Radio                     |
| ------------------ | -------------------------------- | ----------------------------------- |
| Bande de fréquence | 2.4GHz                           | 2.4GHz                              |
| Débit              | 1Mbps ou 2Mbps                   | 1Mbps ou 2Mbps                      |
| Encryption         | Non                              | Non                                 |
| Cannaux            | 81 (0..80)                       | 101 (0..100)                        |
| Group codes        | 255                              | 255                                 |
| Taille des message | 32 (standard) 255 (si configuré) | 32 (standard) 255 (if reconfigured) |
| Bluetooth          | Bluetooth 5.1 *low energy* BLE   | Bluetooth 4.1 *low energy* BLE      |
| Largeur de band BT | 2.4GHz..2.41GHz                  | 2.4GHz..2.41GHz                     |


### Capteurs *et un peu plus*

| Capteur               | Remarque                                                          |
| --------------------- | ----------------------------------------------------------------- |
| Boutons poussoir      |                                                                   |
| Afficheur led         | matrice 5x5, 255 niveau d'intensité par led                       |
| Capteur de luminosité | 10 niveaux mesurables. Utilise la matrice de LED.                 |
| Accéléromètre 3 axes  | plage de fonctionnement 2/4/8/16g. Détection "Free fall"           |
| Boussole              | Nécessite calibration                                             |
| Température           | Entre -40°C et 105°C, non calibré (+/- 5°C). Sonde du processeur. |
| Haut parleur          | 80dB @ 5V, 10cm                                                   |
| Microphone            | MEMs 100Hz - 80kHz                                                |


**Brochage** [^connecteur]
Un outil interactif permet de faciliter la prise en main du brochage [https://microbit.pinout.xyz/](https://microbit.pinout.xyz/)


 
![Brochage](img/edge_connector.svg){width=150 align=right}
![Brochage](img/edge-connector-2.svg){width=150 align=right}

| Fonction          | Disponibilité                           |
| ----------------- | --------------------------------------- |
| GPIO              | 19                                      |
| Multiplexage led  | 6                                       |
| Bouton            | 2                                       |
| E/S numérique     | 19                                      |
| PWM               | 3 max                                   |
| UART              | 1 max                                   |
| Entrée analogique | 6 (10 bits)                             |
| SPI               | 1                                       |
| I2C               | 1 :warning: maitre seulement avec micropython |
| Capteur capacifit | 3                                       |

[^bbc_micro]: [https://fr.wikipedia.org/wiki/BBC_Micro](https://fr.wikipedia.org/wiki/BBC_Micro)
[^bbc_microbit]: [https://fr.wikipedia.org/wiki/Micro:bit](https://fr.wikipedia.org/wiki/Micro:bit)
[^hardware]: Description matérielle micro:bit [https://tech.microbit.org/hardware/](https://tech.microbit.org/hardware/)
[^connecteur]: Description du connecteur micro:bit [https://tech.microbit.org/hardware/edgeconnector/](https://tech.microbit.org/hardware/edgeconnector)



