# 🐍 Fond Vert
## Introduction à la manipulation

- Full pack 
[:octicons-file-zip-16:{.zip}](assets/img-A-initiation/img-A-initiation.zip)
- sujet
[:fontawesome-solid-file-word:{.docx}](assets/img-A-initiation/A-initiation_traitement_image.docx)
[:fontawesome-solid-file-pdf:{.pdf}](assets/img-A-initiation/A-initiation_traitement_image.pdf)
[:simple-python:](assets/img-A-initiation/eleves.zip)
- corrigé
[:simple-python:](assets/img-A-initiation/correction.zip)

## Incrustation fond vert [^1]

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/JvmuLATaDes?start=110" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

- Full pack 
[:octicons-file-zip-16:{.zip}](assets/img-B-incrustation/img-B-incrustation.zip)
- sujet
[:fontawesome-solid-file-word:{.docx}](assets/img-B-incrustation/B-incrustation.docx)
[:fontawesome-solid-file-pdf:{.pdf}](assets/img-B-incrustation/B-incrustation.pdf)
[:simple-python:](assets/img-B-incrustation/eleve.zip)
- corrigé
[:simple-python:](assets/img-B-incrustation/correction.zip)

[^1]: idée originale de Gérald Bonin

