# 🐍 Activités

## :computer: Exo 1 - Réaliser une publication de bout en bout

1. Charger les carte microbit
2. Créer un fichier main.py
3. Ecrire un enchainement de commande afin de publier la temperature sur l'application MQTT Panel


``` mermaid
sequenceDiagram
    participant DNS as DNS 10
    actor  terminal as terminal 20
    participant  capteur as capteur 30
    participant  MQTT_srv as MQTT_srv 40
    participant  MQTT_passerelle

    terminal ->> DNS : addr capteur1 ?
    DNS ->> terminal : 30

    terminal ->> capteur : temperature ?
    capteur ->> terminal : 27
    

    terminal ->> MQTT_srv : formation/temperature1, 27    
    MQTT_srv ->> MQTT_passerelle : formation/temperature1, 27
    MQTT_passerelle ->> MQTT_srv : OK
    MQTT_srv ->> terminal : OK
    
```

!!! methode "Fichier et documentation"

    La documentation et les fichiers sont disponibles:

    * Release  [https://gitlab.com/bdelaup/unet/-/releases](https://gitlab.com/bdelaup/unet/-/releases)
    * Documentation : [http://bdelaup@gitlab.io/unet]([https://gitlab.com/bdelaup/unet/-/jobs/4230313642/artifacts/download?file_type=archive](http://bdelaup@gitlab.io/unet))
    * Distribution des fichier python v. 1.1.0 [https://gitlab.com/bdelaup/unet/-/jobs/4230313641/artifacts/download?file_type=archive](https://gitlab.com/bdelaup/unet/-/jobs/4230313641/artifacts/download?file_type=archive)


## :computer: Exo 2 - créer un réseau unet_light

Le but est de créer une bibliothèque réseau avec des capteur, un dns et une passerelle mqtt. On retrouvera des serveur et des clients, mais on ne se préoccupe pas de la fiabilité : 
  
* pas de retry
* pas d'acquittement
* tous les noeuds sur la même plage de fréquence.
* pas de gestion des erreur

1. écrire le format des échanges entre chaque type de noeuds, requête et réponse
2. écrire les fichiers main de chaque cartes
3. vérifier le bon fonctionnement.

!!! methode "Outils - méthode"

    Utiliser le site [https://python.microbit.org/v/3](https://python.microbit.org/v/3) pour aller plus vite au but

    Répartissez vous le travail !




