# 🐍 Activités

## Mise en situation

Lors de l'activité [`pandas`](tp_pandas_activites.md)[^pandas], nous avons extrait de table de plusieurs milliers d'enregistrements des informations relatives au capteurs.

[^pandas]: [Activité pandas](tp_pandas_activites.md)

| Nom du capteur | Temperature moyenne | Temperature maximum | Ville     | Longitude | Latitude |
| -------------- | ------------------- | ------------------- | --------- | --------- | -------- |
| tmp1571        | 21.916667           | 29                  | VESOUL    | 6.16667   | 47.6333  |
| tmp18972       | 20.583333           | 27                  | PARIS     | 2.34445   | 48.86    |
| tmp23730       | 23.666667           | 31                  | LYON      | 4.84139   | 45.7589  |
| tmp30548       | 22.416667           | 28                  | BORDEAUX  | -0.566667 | 44.8333  |
| tmp31739       | 24.375000           | 31                  | MARSEILLE | 5.37639   | 43.2967  |
| tmp803         | 18.666667           | 26                  | BESANCON  | 6.03333   | 47.25    |

Il s'agit de placer sur une carte :

![marker](img/folium_marker.png)

## Mise en activité

**Pour chaque question**, créer un fichier `folium_x.py` où `x` est le numéro de la question.

Il s'agit d'écrire un script python en suivant les étapes ci-après

1. créer une carte centrée sur la France et l'enregistrer sous le nom de `carte_capteur.html` [aide](https://www.coordonnees-gps.fr/carte/pays/FR) [^coordonnees] (`carte = folium.Map(...)`)
2. ajouter un marqueur par capteur (`mon_marqueur = folium.Marker(...)`)
3. ajouter, pour chaque marqueur, une étiquette qui s'affiche lors d'un survol de souris (`tooltip = "..."`)
4. ajouter une popup pour afficher le nom du capteur, les température moyenne et maxi lorsque le marqueur est cliqué (`popup= "..."`)
5. personnaliser les marqueurs : couleur et icone [choix](https://getbootstrap.com/docs/3.3/components/#glyphicons)[^icone] (`folium.Icon(color="...", icon="...")`)

[^coordonnees]: [https://www.coordonnees-gps.fr/carte/pays/FR](https://www.coordonnees-gps.fr/carte/pays/FR)
[^icone]: [https://getbootstrap.com/docs/3.3/components/#glyphicons](https://getbootstrap.com/docs/3.3/components/#glyphicons)
