# ✔️ Activité - Correction

Télécharger les fichiers de corrections ici [:fontawesome-solid-download:](tp_pandas_telechargement.md)

## Mise en situation

Nous disposons d'un fichier au format CSV contenant les 36000 commune de France dont les descripteurs sont les suivants :

``` csv title="Descripteurs de villes_france.csv"
departement	
nom	code_poste	
population	
surface	
longitude	
latitude	
altitude_min	
altitude_max	
nom_cpt_temperature	
nom_cpt_lumiere
```

Par ailleurs, nous disposons du relevé de température de plusieurs capteur :
releve_temperature_avril_2023
``` csv title="Descripteurs de releve_temperature_avril_2023.csv"
date nom_capteur temperature
```
L'objectif de l'activité est de :

1. déterminer la température moyenne et maximum de chaque capteur.
2. déterminer ville et les coordonnées GPS de chaque capteur


## Mise en action

### :pencil2: Tableau à compléter

| Nom du capteur | Temperature moyenne | Temperature maximum | Ville     | Longitude | Latitude |
| -------------- | ------------------- | ------------------- | --------- | --------- | -------- |
| tmp1571        | 21.916667           | 29                  | VESOUL    | 6.16667   | 47.6333  |
| tmp18972       | 20.583333           | 27                  | PARIS     | 2.34445   | 48.86    |
| tmp23730       | 23.666667           | 31                  | LYON      | 4.84139   | 45.7589  |
| tmp30548       | 22.416667           | 28                  | BORDEAUX  | -0.566667 | 44.8333  |
| tmp31739       | 24.375000           | 31                  | MARSEILLE | 5.37639   | 43.2967  |
| tmp803         | 18.666667           | 26                  | BESANCON  | 6.03333   | 47.25    |

### :computer: Exo 1 - déterminer les valeurs moyennes et max de chaque capteur

1. Charger le fichier `releve_temperature_avril_2023.csv` dans la variable `temperatures`
``` py linenums="1"
from pandas import read_csv

# Lire un fichier au format CSV
fichier_temp = "releve_temperature_avril_2023.csv"
temperatures = read_csv(fichier_temp, sep=";")
```
2. exécuter la commande `temperatures.groupby(by="nom_capteur").groups.keys()` et noter le nom 
``` py linenums="1"
# Récupérer la valeur des groupe
groupes_capteur = temperatures.groupby(by="nom_capteur").groups.keys()
print(groupes_capteur) 
# tmp1571 tmp18972 tmp23730 tmp30548 tmp31739 tmp803
```
3. Pour chaque capteur récupérer la température maximum et la noter.
``` py linenums="1" 
# Pour chaque capteur la temperature maximum
#   Version 1
print("tmp1571 :", temperatures.loc[temperatures["nom_capteur"] == "tmp1571"].temperature.max())
print("tmp18972 :", temperatures.loc[temperatures["nom_capteur"] == "tmp18972"].temperature.max())
print("tmp23730 :", temperatures.loc[temperatures["nom_capteur"] == "tmp23730"].temperature.max())
print("tmp30548 :", temperatures.loc[temperatures["nom_capteur"] == "tmp30548"].temperature.max())
print("tmp31739 :", temperatures.loc[temperatures["nom_capteur"] == "tmp31739"].temperature.max())
print("tmp803 :", temperatures.loc[temperatures["nom_capteur"] == "tmp803"].temperature.max())
print("\n")

#   Version 2
liste_nom_capteurs = ["tmp1571", "tmp18972", "tmp23730", "tmp30548", "tmp31739", "tmp803"]
for nom in liste_nom_capteurs:
    temp_max = temperatures.loc[temperatures["nom_capteur"] == nom].temperature.max()
    print(nom+" :", temp_max)
print("\n")

#   Version 3
temp_max = temperatures.groupby(by="nom_capteur").max()
print(temp_max, "\n")
```
4. Pour chaque capteur récupérer la température moyenne et la noter
``` py linenums="1" 
# Pour chaque capteur la temperature moyenne
temp_moy = temperatures.groupby(by="nom_capteur").mean(numeric_only=True)
print(temp_moy, "\n")
```
## :computer: Exo 2 - Localisation
1. Charger le fichier `villes_france.csv` dans la variable `villes`
``` py linenums="1"
from pandas import read_csv

# Lire un fichier au format CSV
fichier_temp = "villes_france.csv"
villes = read_csv(fichier_temp, sep=";")
print (villes.columns, "\n")

```
2. Pour chaque capteur (tmp1571, tmp18972, tmp23730, tmp30548, tmp31739, tmp803), récupérer le nom de la ville où il se situe et le noter.
``` py linenums="1"
# Nom de villes
capteurs = ["tmp1571", "tmp18972", "tmp23730", "tmp30548", "tmp31739", "tmp803"]
for capteur in capteurs:
    villes_capteur = villes.loc[(villes["nom_cpt_temperature"]== capteur),["nom","nom_cpt_temperature"]]
    print (villes_capteur, "\n")
```
3. Pour chaque capteur, récupérer les coordonnées GPS et les noter
``` py linenums="1"
# Coordonnées
capteurs = ["tmp1571", "tmp18972", "tmp23730", "tmp30548", "tmp31739", "tmp803"]
for capteur in capteurs:
    villes_capteur = villes.loc[(villes["nom_cpt_temperature"]== capteur),["nom_cpt_temperature","longitude","latitude"]]
    print (villes_capteur, "\n")
```

