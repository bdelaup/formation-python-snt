# Les outils de développement
## Les environnements de développement python
### Python-micro:bit en ligne [^python_mb]
C'est l'éditeur python officiel du projet micro:bit.
Il est entièrement en ligne. Le site permet de télécharger les fichiers et l'afficher le terminal directement depuis la page web.

![mpy](img/mpy_mb_accueil.drawio.png)

1. Zone de codage
2. Gestion de projet (si plusieurs fichiers)
3. Documentation et exemples
4. Simulateur
5. Connexion avec la carte, téléversement

:warning: Seuls les navigateurs basés sur chromium fonctionnent : chrome, edge.

Il est possible d'intégrer les exemples au code par glisser déposé depuis la documentation à gauche, se rapprochant ainsi de l'ergonomie du codage par bloc.

Lorsque l'on exporte le fichier hex, celui-ci contient à la fois le programme binaire de `micropython` mais également les fichiers python que vous avez créés.

### Thonny

Cet outils est supporté officiellement par la fondation Raspberry. Il peut être utiliser pour éditer du code micro:bit (micropython y est très bien intégré) ou du code PC. Il permet de gérer simplement les bibliothèque tierce partie.

![thonny](img/mb_thonny.drawio.png)

1. Zone de codage
2. Gestionnaire de fichier PC
3. Gestionnaire de fichier carte microbit
4. Concole
5. Choix de l'interpreteur (PC ou microbit)

Pour ajouter des bibliothèque :

![mpy](img/thonny_paquet.png){width=300}


### Vittascience[^vittascience] + Cabristeam[^cabristeam]

Le site vitascience offre offre une large gamme d'outils pour l'apprentissage de la programmation (notement microbit mais pas seulement).
[https://fr.vittascience.com/code](https://fr.vittascience.com/code)


Le territoire numérique éducatif permet d'accéder à l'interface de gestion de classe de vittascience. Pour y avoir accé, il faut 

- se rendre sur le site [https://tne.reseau-canope.fr/outils-numeriques/solutions-numeriques-educatives.html](https://tne.reseau-canope.fr/outils-numeriques/solutions-numeriques-educatives.html)
- chercher `Cabri STEAM Lycée` 
- suivre la formation (30 minutes)
- demander l'activation de Cabri Steam dans le GAR d'Eclat-BFC

### Makecode[^makecode] 

Outil en ligne de Microsoft avec un simulateur. Ne permet pas de programmer en python l'interface python n'est pas vraiment du python, je la déconseille. Par contre, l'interface en block est très séduisante.

[https://makecode.microbit.org/](https://makecode.microbit.org/).

## Les terminaux
Si vous souhaitez utiliser un terminal pour intéragir avec la console sans utiliser d'environnement de développement, n'importe quel émulateur de terminal fonctionne :

* putty[^putty]
* teraterm[^teraterm] 
* Automated serial terminal[^serial_port_term]

La configuration série est : 
* 115200 bauds
* 8 bits de données
* Pas de bits de control
* 1 bit stop

Si nécessaire, possible et que ça ne fonctionne pas, activer le DTR et le RTS.



[^python_mb]: [https://python.microbit.org/v/3/](https://python.microbit.org/v/3/)
[^vittascience]: [https://fr.vittascience.com/code](https://fr.vittascience.com/code)
[^makecode]: [https://makecode.microbit.org/](https://makecode.microbit.org/)
[^putty]: [https://www.putty.org/](https://www.putty.org/)
[^teraterm]: [https://ttssh2.osdn.jp/index.html.en](https://ttssh2.osdn.jp/index.html.en)
[^serial_port_term]: [https://freeserialportterminal.com/](https://freeserialportterminal.com/)