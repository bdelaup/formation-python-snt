import folium

carte = folium.Map(
    location=[46.2322, 2.2096], # Position degrée décimaux (DD)
    zoom_start = 6              # Zoom 3:monde, 6:pays, 13:ville
    )

mon_marqueur = folium.Marker(
    location = [46.2, 2.2],
    icon = folium.Icon(color="red", icon="info-sign"),
    tooltip = "Centre",
    popup="capteur:tmp1571 max:29 moyenne:21.9"
).add_to(carte)

carte.save("intro3.html")


