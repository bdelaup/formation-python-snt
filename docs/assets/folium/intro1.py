import folium

# Carte centrée sur la France
carte = folium.Map(
    location=[46.2322, 2.2096], # Position degrée décimaux (DD) (centré sur la france)
    zoom_start = 6              # Zoom 3:monde, 6:pays, 13:ville
    )

# Ajoute un marqueur d'exemple
position_marqueur = [46.2, 2.2] # Position degrée décimaux (DD)
marqueur = folium.Marker(
    location=position_marqueur
    )

# Ajouter le marqueur à la carte
marqueur.add_to(carte)

# Enregistre la carte
carte.save("intro1.html")