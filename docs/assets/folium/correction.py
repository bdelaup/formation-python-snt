import folium

# Carte centrée sur la France
carte = folium.Map(
    location=[46.2322, 2.2096], # Position degrée décimaux (DD)
    zoom_start = 6             # Zoom 3:monde, 6:pays, 13:ville
    )

# Ajoute exemple
folium.Marker(
    location=[46.2, 2.2],
    popup="Simple_marker Ligne-1, Ligne-2",
).add_to(carte)

# Ajoute Vesoul
popup_vesoul =folium.Popup("VESOUL capteur:tmp1571 max:29 moyenne:21.9")
icon_vesoul = folium.Icon(color="red", icon="info-sign")
tooltip_vesoul = "VESOUL"
position_vesoul = [47.6333, 6.16667]

folium.Marker(
    location=position_vesoul,
    popup=popup_vesoul,
    tooltip= tooltip_vesoul,
    icon=icon_vesoul,
).add_to(carte)

# Ajoute Paris
folium.Marker(
    location=[48.86, 2.34445],
    tooltip="PARIS",
    popup="PARIS capteur:tmp18972 max:27 moyenne:20.6",
    icon=folium.Icon(color="green", icon="glyphicon-picture"),
).add_to(carte)

# Ajoute Lyon
folium.Marker(
    location=[45.7589, 4.84139],
    tooltip="LYON",
    popup="LYON capteur:tmp23730 max:31 moyenne:23.6",
    icon=folium.Icon(color="purple", icon="glyphicon-pencil"),
).add_to(carte)

# Ajoute Bordeau
folium.Marker(
    location=[44.8333, -0.566667],
    tooltip="BORDEAUX",
    popup="BORDEAUX capteur:tmp30548 max:28 moyenne:22.4",
    icon=folium.Icon(color="blue", icon="glyphicon-road"),
).add_to(carte)

# Ajoute Marseille
folium.Marker(
    location=[43.2967 , 5.37639],
    tooltip="MARSEILLE",
    popup="MARSEILLE capteur:tmp31739 max:31 moyenne:24.3",
    icon=folium.Icon(color="gray", icon="glyphicon-lock"),
).add_to(carte)

# Ajoute Besancon
folium.Marker(
    location=[47.25, 6.03333],
    tooltip="BESANCON",
    popup="BESANCON capteur:tmp803 max:26 moyenne:18.6",
    icon=folium.Icon(color="orange", icon="glyphicon-tint"),
).add_to(carte)




carte.save("carte_capteur.html")


