import folium

# Carte centrée sur la France
carte = folium.Map(
    location=[46.2322, 2.2096], # Position degrée décimaux (DD)
    zoom_start = 6              # Zoom 3:monde, 6:pays, 13:ville
    )

# Créer un marqueur
ma_position = [46.2, 2.2] # position
mon_icone = folium.Icon(color="red", icon="info-sign") # Apparence
mon_tooltip = folium.Tooltip("Centre") # Survol
mon_popup = folium.Popup("capteur:tmp1571 max:29 moyenne:21.9") # Texte

mon_marqueur = folium.Marker(
    location=ma_position,
    icon=mon_icone,
    tooltip = mon_tooltip,
    popup=mon_popup
)

# Ajouter le marqueur
mon_marqueur.add_to(carte)
carte.save("intro2.html")


