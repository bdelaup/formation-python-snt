import folium

# https://getbootstrap.com/docs/3.3/components/#glyphicons
# https://www.coordonnees-gps.fr/carte/pays/FR

# Carte centrée sur la France
carte = folium.Map(
    location=[46.2322, 2.2096], # Position degrée décimaux (DD)
    zoom_start = 6              # Zoom 3:monde, 6:pays, 13:ville
    )

# Ajoute un marqueur d'exemple
position_marqueur = [46.2, 2.2] # Position degrée décimaux (DD)

marqueur = folium.Marker(location=position_marqueur)
marqueur.add_to(carte)

# marqueur = folium.Marker(location=, popup="Simple_marker Ligne-1, Ligne-2")
# Tooltip
# marqueur.add_to(carte)

carte.save("intro.html")