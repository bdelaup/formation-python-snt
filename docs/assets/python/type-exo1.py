def milieu (A, B):
    """ 
    milieu (tuple , tuple ) -> tuple

    retourne les coordoonnées du milieu du segment [AB], 
        où A et B sont des tuples
        (on identifie le point avec ses coordonnées ) 
    """
    A_x = A[0]
    A_y = A[1]
    B_x = B[0]
    B_y = B[1]
    
    return ((A_x+B_x)/2, (A_y+B_y)/2)
    
A = (0, 0)
B = (2, 2)

print(milieu (A, B))
