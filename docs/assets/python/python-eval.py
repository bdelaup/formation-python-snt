import turtle
from turtle import *

up()
goto(-200,-200)
left(90)
down()

def etoile(nb_branche):
    lg_branche = 50 + nb_branche
    if nb_branche < 5:
        color("black")
    elif nb_branche < 10:
        color("red")
    elif nb_branche < 50:
        color("blue")
    else:
        color("green")
        
    for _ in range(nb_branche):
        forward(lg_branche)
        backward(lg_branche)
        left(360 / nb_branche)
    forward(100)
        
etoile(3)
etoile(7)
etoile(30)
etoile(9)
etoile(100)

turtle.getscreen().getcanvas().postscript(file='outputname.ps')


exitonclick()