from pandas import read_csv

# Lire un fichier au format CSV
fichier_villes = "villes_france.csv"
villes = read_csv(fichier_villes, sep=";")

# Afficher la population maximum 
print ("=== Population maximum ===")
population_max = villes.population.max()
print(population_max)

# Rechercher la ville avec la population max (2243833)
print ("=== Ville de population maximum ===")
ville_population_max = villes.loc[(villes["population"] == 2243833)]
print(ville_population_max.nom)

# Moyenne de population
print ("=== Population moyenne ===")
population_moyenne= villes.population.mean()
print(population_moyenne)