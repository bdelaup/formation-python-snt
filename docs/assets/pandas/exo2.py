from pandas import read_csv

# Lire un fichier au format CSV
fichier_temp = "villes_france.csv"
villes = read_csv(fichier_temp, sep=";")
print (villes.columns, "\n")

# Nom de villes
capteurs = ["tmp1571", "tmp18972", "tmp23730", "tmp30548", "tmp31739", "tmp803"]
for capteur in capteurs:
    villes_capteur = villes.loc[(villes["nom_cpt_temperature"]== capteur),["nom","nom_cpt_temperature"]]
    print (villes_capteur, "\n")
    
# Coordonnées
capteurs = ["tmp1571", "tmp18972", "tmp23730", "tmp30548", "tmp31739", "tmp803"]
for capteur in capteurs:
    villes_capteur = villes.loc[(villes["nom_cpt_temperature"]== capteur),["nom_cpt_temperature","nom","longitude","latitude"]]
    print (villes_capteur, "\n")
    
