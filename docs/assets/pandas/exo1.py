from pandas import read_csv

# Lire un fichier au format CSV
fichier_temp = "releve_temperature_avril_2023.csv"
temperatures = read_csv(fichier_temp, sep=";")

# Récupérer la valeur des groupe
groupes_capteur = temperatures.groupby(by="nom_capteur", as_index=False).groups.keys()
print(groupes_capteur, "\n")

# Pour chaque capteur la temperature maximum
#   Version 1
print("tmp1571 :", temperatures.loc[temperatures["nom_capteur"] == "tmp1571"].temperature.max())
print("tmp18972 :", temperatures.loc[temperatures["nom_capteur"] == "tmp18972"].temperature.max())
print("tmp23730 :", temperatures.loc[temperatures["nom_capteur"] == "tmp23730"].temperature.max())
print("tmp30548 :", temperatures.loc[temperatures["nom_capteur"] == "tmp30548"].temperature.max())
print("tmp31739 :", temperatures.loc[temperatures["nom_capteur"] == "tmp31739"].temperature.max())
print("tmp803 :", temperatures.loc[temperatures["nom_capteur"] == "tmp803"].temperature.max())
print("\n")

#   Version 2
liste_nom_capteurs = ["tmp1571", "tmp18972", "tmp23730", "tmp30548", "tmp31739", "tmp803"]
for nom in liste_nom_capteurs:
    temp_max = temperatures.loc[temperatures["nom_capteur"] == nom].temperature.max()
    print(nom+" :", temp_max)
print("\n")

#   Version 3
temp_max = temperatures.groupby(by="nom_capteur").max()
print(temp_max, "\n")

# Pour chaque capteur la temperature moyenne
temp_moy = temperatures.groupby(by="nom_capteur").mean(numeric_only=True)
print(temp_moy, "\n")
