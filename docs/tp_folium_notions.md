# 🕮 Notions
## Introduction
`Folium`[^folium] est une bibliothèque de cartographie. C'est une surcouche de la bibliothèque javascript[^leaflet] utilisée pour le rendu de carte dans les page web.

Il est possible d'utiliser `folium` derrière une application web en python/flask,  mais on peut aussi simplement s'en servir pour générer une *simple* page html.

## Installation

![install](img/folium_installation.drawio.png){width=500}

## Principe

Principe de construction d'une carte en python avec `folium`
1. construire un objet carte
2. construire des objets marqueurs
3. associer les marqueurs à la carte
4. sauvegarder la carte.

Ce qui donne en python :
``` py linenums="1" title="intro1.py"
import folium

# Carte centrée sur la France
carte = folium.Map(
    location=[46.2322, 2.2096], # Position degrée décimaux (DD) (centré sur la france)
    zoom_start = 6              # Zoom 3:monde, 6:pays, 13:ville
    )

# Ajoute un marqueur d'exemple
position_marqueur = [46.2, 2.2] # Position degrée décimaux (DD)
marqueur = folium.Marker(
    location=position_marqueur
    )

# Ajouter le marqueur à la carte
marqueur.add_to(carte)

# Enregistre la carte
carte.save("intro1.html")
```
<iframe width="400" height="200" src="assets/folium/intro1.html" title="intro1.html" frameborder="1"></iframe>

## Création d'un marqueur

**Objectif :**

<iframe width="400" height="200" src="assets/folium/intro2.html" title="intro2.html" frameborder="1"></iframe>

### Version longue
``` py linenums="1" title="intro2.py"
import folium

# Carte centrée sur la France
carte = folium.Map(
    location=[46.2322, 2.2096], # Position degrée décimaux (DD)
    zoom_start = 6             # Zoom 3:monde, 6:pays, 13:ville
    )

# Créer un marqueur
ma_position = [46.2, 2.2] # position
mon_icone = folium.Icon(color="red", icon="info-sign") # Apparence
mon_tooltip = folium.Tooltip("Centre") # Survol
mon_popup = folium.Popup("capteur:tmp1571 max:29 moyenne:21.9") # Texte

mon_marqueur = folium.Marker(
    location=ma_position,
    icon=mon_icone,
    tooltip = mon_tooltip,
    popup=mon_popup
)

# Ajouter le marqueur
mon_marqueur.add_to(carte)
carte.save("intro2.html")
```
### Version condensée
``` py linenums="1" title="intro3.html" 
import folium

carte = folium.Map(
    location=[46.2322, 2.2096], # Position degrée décimaux (DD)
    zoom_start = 6              # Zoom 3:monde, 6:pays, 13:ville
    )

mon_marqueur = folium.Marker(
    location = [46.2, 2.2],
    icon = folium.Icon(color="red", icon="info-sign"),
    tooltip = "Centre",
    popup="capteur:tmp1571 max:29 moyenne:21.9"
).add_to(carte)

carte.save("intro3.html")
```
## Personnalisation des marqueurs
Il est possible de personnaliser la forme et la couleur des marqueur :

``` py linenums="1" title="personnalisation des marqueurs" hl_lines="3"
mon_marqueur = folium.Marker(
    location = [46.2, 2.2],
    icon = folium.Icon(color="red", icon="info-sign"),
    tooltip = "Centre",
    popup="capteur:tmp1571 max:29 moyenne:21.9"
).add_to(carte)
```
Une liste d’icônes facilement utilisable est disponible sur le site de `bootstrap`[^icone].

## Intégration dans un site de la carte générée à l'aide d'une iframe

Pour intégrer une carte présente dans le même répertoire qu'un fichier `.html`, il suffit d'intégrer le code suivant : 

``` html linenums="1" title="Intégration de la carte à une page web"  
<iframe width="400" height="200" src="intro1.html" title="intro1.html" frameborder="1"></iframe>
```
## Coordonnées gps

Par défaut, `Folium` travail avec des coordonnées au format *Position degrée décimaux (DD)*.

Pour déterminer les coordonnées GPS d'un site, il est possible d'utiliser le si [coordonnees-gps.fr](https://www.coordonnees-gps.fr/carte/pays/FR)[^coordonnees]

[^folium]: [https://python-visualization.github.io/folium/](https://python-visualization.github.io/folium/)
[^leaflet]: [https://leafletjs.com/](https://leafletjs.com/) 
[^icone]: [https://getbootstrap.com/docs/3.3/components/#glyphicons](https://getbootstrap.com/docs/3.3/components/#glyphicons)
[^coordonnees]: [https://www.coordonnees-gps.fr/carte/pays/FR](https://www.coordonnees-gps.fr/carte/pays/FR)
