# ✔️ Activité - Correction

## :computer: Exo 1 - Réaliser une publication de bout en bout - Correction

1. Charger les carte microbit -> [https://bdelaup.gitlab.io/unet/unet_installation.html](https://bdelaup.gitlab.io/unet/unet_installation.html)
2. Créer un fichier main.py -> [https://bdelaup.gitlab.io/unet/unet_api.html#description-de-lapi](https://bdelaup.gitlab.io/unet/unet_api.html#description-de-lapi) (sans lire la correction)

``` py linenums="1" title="Exo 1 : solution"

from unet_fonction import initialisation_reseau, 
from unet_fonction import requete_dns_adresse
from unet_fonction import requete_capteur
from unet_fonction import publication_mqtt

from config import ADR_IP, ADR_SRV_DNS 

# initialisation du réseau avec l'adresse ip de la carte
# et celle du serveur DNS
initialisation_reseau(ADR_IP, ADR_SRV_DNS)

# Récupération de l'adresse du capteur 1
adresse_capteur = requete_dns_adresse("capteur1")
print ("Adresse capteur", adresse_capteur)

# Récupération de la température du capteur 1
temperature = requete_capteur(adresse_capteur, "temperature")
print ("Température", temperature)

# Récupération de l'adresse de la passerelle MQTT
adresse_passerelle_mqtt = requete_dns_adresse("mqtt")
print ("Adresse mqtt", adresse_passerelle_mqtt)

# publication de la temperature
publication_mqtt(adresse_passerelle_mqtt, "capteur1/temperature", temperature)
```

## :computer: Exo 2 - créer un réseau - Correction

Correction sur demande !
