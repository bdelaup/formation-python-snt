# 🍺 Scratch <> Python

![](img/python-scratch_python/image1.png){width="500"}

##  Affectation

Affectations de valeur à une variable `x ← 2`
=== "Scratch"
	![](img/python-scratch_python/image2.png){width="175"} 
=== "python"
	``` py
	x = 2
	```


La variable y prend la valeur 5 × (2 - 3)  `y←5×(2-3)`
=== "Scratch"
	![](img/python-scratch_python/image4.png){width="300"}
=== "python"
	```
	y = 5 *(2-3)
	```

La variable somme prend la valeur x + y `Somme ← x+y`

=== "Scratch"
	![](img/python-scratch_python/image6.png){width="300"}
=== "python"
	``` py
	somme = x + y
	```

La variable prenom prend la valeur "Lucien" `Prenom ← "Lucien"`

=== "Scratch"
	![](img/python-scratch_python/image8.png){width="300"}
=== "python"
	``` py
	prenom = "Lucien"


	```

`identite ← prenom + nom`
=== "Scratch"
	![](img/python-scratch_python/image10.png){width="400"}
=== "python"
	``` py
	prenom = "Lucien"
	nom = "Dubois"
	identite = prenom + nom
	```

## Instruction conditionnelle "si"

`si -- alors -- (sinon)`
```
Si d < 0 
	Alors afficher "La température est négative"
finSi
```

=== "Scratch"
	![](img/python-scratch_python/image11.png){width="250"}
=== "python"
	``` py
	if d <0 :
		print ("la temperature est négative")
	```

On note le décalage vers la droite de l\'instruction \"print\" par rapport à celle du \"if\": cette indentation permet de définir le bloc d\'instructions à exécuter si la condition \"d\<0\" est vérifiée; elle est alors précédée du symbole \":\".

```
Si A = B  
	Alors afficher "L'égalité est vérifiée"
Sinon 
	afficher "L'égalité n'est pas vérifiée"
```

=== "Scratch"
	![](img/python-scratch_python/image13.png){width="250"}

=== "python"
	``` py
	if a == b:
		print("L'égalité vérifiée")
	else :
		print("L'égalité n'est pas vérifiée")
	```

## Instruction boucle bornée « pour »
`faire n fois ...`
```
F ← 1     

Pour n allant de 1 à 5       
	F ← f*n  
FinPour   

Afficher "Factorielle de 5 est égale à "
Afficher f
```

=== "Scratch"
	![](img/python-scratch_python/image15.png){width="250"}
=== "python"
	``` py
	n = 0
	f = 0

	for i in range (4):
		n = n + 1
		f = f *n
	
	print ("Factoriel de 5 est égale à : ", f)
	```

## Instruction de boucle non bornée « tant que »
`tant que ... vrai, faire ...`

```

Demander n                       
l ← 0                            
Tant que n ≥ 0                   
  n ← ( n -- (n modulo 10)) / 10 
  l ← l +1                       
finTantQue                       
Afficher l                       


```
=== "Scratch"
	![](img/python-scratch_python/image17.png){width="350"}

=== "python"
	``` py
	n = int(input("Entrer la valeur de n"))
	while n != 0:
		n = (n-(n%10)) / 10
		i = i + 1
	print ("Le chiffre n est ...")
	sleep(2)
	print (i)	
	```

## Saisie utilisateur et fonctions

![](img/python-scratch_python/image19.png){width="600"}


