# 🕮 Notions

Télécharger les fichiers d'exemple ici [:fontawesome-solid-download:](tp_pandas_telechargement.md)


## Des données et des traitements

Vidéo allo la hotline[^allo_hotline_donnees].

<iframe width="400" height="200" src="https://www.youtube-nocookie.com/embed/IJJgcZ2DEs0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Vocabulaire

![tables](img/pandas_vocabulaire.drawio.png){width=500}

## Format CSV

Exemple[^wiki_table] : 

``` csv title="Contenu du fichier liste.csv" linenums="0"
Sexe,Prénom,Année de naissance
M,Alphonse,1932
F,Béatrice,1964
F,Charlotte,1988
```

En voici une représentation tabulaire.

| Sexe | Prénom    | Année de naissance |
| ---- | --------- | ------------------ |
| M    | Alphonse  | 1932               |
| F    | Béatrice  | 1964               |
| F    | Charlotte | 1988               |

Dans cette exemple, le séparateur est une virgule. Il n'est pas rare que ce soit un `;` à la place. C'est le séparateur par défaut d'Excel.


Pour importer un fichier CSV dans excel, s'il le délimiteur n'est pas détecté:
<iframe width="400" height="200" src="https://www.youtube-nocookie.com/embed/nxYhWhiA9vQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Pandas
`pandas` est une bibliothèque python facilitant le traitement de données, en particulier celles en tables.

Pour plus d'aide, le *cheat sheet* de `pandas`: [:fontawesome-solid-file-pdf:{.pdf}](assets/pandas/Pandas_Cheat_Sheet.pdf)[^pandas_cheat]


### Le vocabulaire de pandas

* table -> dataframe
* objet -> series
* descripteur -> header


### Installer pandas

Pour installer `pandas`[^install_pandas] dans `thonny`:

<iframe src="https://www.veed.io/embed/a9ecd4bb-1c48-4b6e-b0b1-8530f21e50d6" width="400" height="200" frameborder="0" title="Installer pandas" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


### Manipulation de base

``` py linenums="1" title="Manipulations de base : selection, filtre"
from pandas import read_csv

# Lire un fichier au format CSV
fichier_villes = "villes_france.csv"
villes = read_csv(fichier_villes, sep=";")

# Afficher les données pour une colonne
print ("=== Afficher la table ===")
print(villes)

# Afficher le nom des colonnes
print ("=== Afficher le nom des colonnes ===")
print(villes.columns)

# Afficher les données pour une colonne
print ("=== Afficher le nom des première villes ===")
print(villes.nom.head())

# Afficher l'objet correspondant à la ville de Vesoul
print ("=== Afficher vesoul ===")
vesoul = villes.loc[(villes["nom"]=="VESOUL")]
print (vesoul)

# Lire le nom du capteur de temperature de la ville de vesoule
print ("=== Nom du capteur de température ===")
nom_capteur = vesoul["nom_cpt_temperature"]
print(nom_capteur)

```

``` py linenums="1" title="Manipulations de base : moyenne, max"
from pandas import read_csv

# Lire un fichier au format CSV
fichier_villes = "villes_france.csv"
villes = read_csv(fichier_villes, sep=";")

# Afficher la population maximum 
print ("=== Population maximum ===")
population_max = villes.population.max()
print(population_max)

# Rechercher la ville avec la population max (2243833)
print ("=== Ville de population maximum ===")
ville_population_max = villes.loc[(villes["population"] == 2243833)]
print(ville_population_max.nom)

# Moyenne de population
print ("=== Population moyenne ===")
population_moyenne= villes.population.mean()
print(population_moyenne)

# Regrouper les ville par département et n'afficher que le nom du département
print ("=== Numéro des départements ===")
departements = villes.sort_values("departement", ascending=False).groupby(by="departement").groups.keys()
print(departements)

# Afficher les 10 villes les plus peuplées
print ("=== Trier par population croissante ===")
villes_10 = villes.sort_values("population", ascending=False)[["nom", "population"]].head(10)
print(villes_10)
```

### A comprendre & retenir

- accéder à une table `villes = read_csv("villes_france.csv", sep=";")`
- sélectionner un objet `vesoul = villes.loc[(villes["nom"]=="VESOUL")]`
    - `loc` pour rechercher
    - `villes["nom"]` pour choisir le descripteur
    - `=="VESOUL"` pour filtrer
- afficher la données d'un objet `vesoul.population`
    - `vesoul` est l'objet
    - `.population` est une données
- effectuer des calculs sur une colonne entière `villes.population.max()`
    - `villes` est une table
    - `.population` représente la colonne
    - `.max()` est la fonction d’agrégation.
- Enchainer les opérations `villes.sort_values("population", ascending=False)[["nom", "population"]].head(10)`
    - `villes` est une table
    - `.sort_values("population", ascending=False)` est une table triée par ordre décroissant de population
    - `[["nom", "population"]]` selectionne uniquement le nom et la population
    - `.head(10)` affiche les 10 premier résultats uniquement

Pour plus d'aide, le *cheat sheet* de `pandas`: [:fontawesome-solid-file-pdf:{.pdf}](assets/pandas/Pandas_Cheat_Sheet.pdf)[^pandas_cheat]

[^allo_hotline_donnees]: Allo la hotline [https://youtu.be/IJJgcZ2DEs0](https://youtu.be/IJJgcZ2DEs0)
[^wiki_table]: Wikipedia format `csv` [https://fr.wikipedia.org/wiki/Comma-separated_values](https://fr.wikipedia.org/wiki/Comma-separated_values)
[^install_pandas]: Installer `pandas` [https://veed.io/view/a9ecd4bb-1c48-4b6e-b0b1-8530f21e50d6](https://veed.io/view/a9ecd4bb-1c48-4b6e-b0b1-8530f21e50d6)
[^pandas_cheat]: Feuille de triche pandas [https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf](https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf)
