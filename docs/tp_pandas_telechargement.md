# 📥 Téléchargements

Full pack [:octicons-file-zip-16:{.zip}](assets/pandas/pandas.zip)

**Fichiers de données**

- releve_temperature_avril_2023.csv [:material-file-table:{.csv}](assets/pandas/releve_temperature_avril_2023.csv)
- villes_france.csv [:material-file-table:{.csv}](assets/pandas/villes_france.csv)

**Exemples**

- manipulation_base_selection_filtre.py [:simple-python:{.py}](assets/pandas/manipulation_base_selection_filtre.py)
- manipulation_base_max_moy.py [:simple-python:{.py}](assets/pandas/manipulation_base_max_moy.py)

**Correction**

- exo1.py [:simple-python:{.py}](assets/pandas/exo1.py)
- exo2.py [:simple-python:{.py}](assets/pandas/exo2.py)

**Feuille de triche pandas** [:fontawesome-solid-file-pdf:{.pdf}](assets/pandas/Pandas_Cheat_Sheet.pdf)


