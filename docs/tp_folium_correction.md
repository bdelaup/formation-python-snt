# ✔️ Activité - Correction



"carte_capteur.html"

1. créer une carte centrée sur la France 
``` py linenums="1" title="Exo1"
carte = folium.Map(
    location=[46.2322, 2.2096], 
    zoom_start = 6             
    )
carte.save("carte_capteur.html")
```
1. ajouter un marqueur par capteur 
``` py linenums="1" title="Exo2"
# inclure le code de la question 1

folium.Marker( location=[47.6333, 6.16667]l).add_to(carte) # Ajoute Vesoul
folium.Marker( location=[48.86, 2.34445]).add_to(carte)    # Ajoute Paris
folium.Marker( location=[45.7589, 4.84139]).add_to(carte)  # Ajoute Lyon
folium.Marker( location=[44.8333, -0.566667]).add_to(carte) # Ajoute Bordeau
folium.Marker( location=[43.2967 , 5.37639]).add_to(carte)  # Ajoute Marseille
folium.Marker( location=[47.25, 6.03333]).add_to(carte)     # Ajoute Besancon
```
3. ajouter, pour chaque marqueur, une étiquette qui s'affiche lors d'un survol de souris 
``` py linenums="1" title="étiquettes"
folium.Marker(location=[47.6333, 6.16667], tooltip="VESOUL").add_to(carte)
```
4. ajouter une popup pour afficher le nom du capteur, les température moyenne et maxi lorsque le marqueur est cliqué
``` py linenums="1" title="popup" hl_lines="5"
# Ajoute Paris, a repeter pour chaque ville
folium.Marker(
    location=[48.86, 2.34445],
    tooltip="PARIS",
    popup="PARIS capteur:tmp18972 max:27 moyenne:20.6",
).add_to(carte)
```
5. personnaliser les marqueurs : couleur et icone 
``` py linenums="1" title="popup"
# à rajouter au code des question précédentes
icon = folium.Icon(color="green", icon="glyphicon-picture")
icon = folium.Icon(color="purple", icon="glyphicon-pencil")
icon = folium.Icon(color="red", icon="info-sign")
icon = folium.Icon(color="blue", icon="glyphicon-road")
icon = folium.Icon(color="gray", icon="glyphicon-lock")
icon=  folium.Icon(color="orange", icon="glyphicon-tint")
```

<iframe width="500" height="500" src="assets/folium/carte_capteur.html" title="carte_capteur.html" frameborder="1"></iframe>


