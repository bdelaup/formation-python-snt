# 🕮 Notions - Bases 

## Activité d'introduction

Full pack [:octicons-file-zip-16:{.zip}](assets/python/python.zip)

**Cours / synthèse**

- élève
[:fontawesome-solid-file-word:{.docx}](assets/python/python_algo%20-%20synthese.docx)
[:fontawesome-solid-file-pdf:{.pdf}](assets/python/python_algo%20-%20synthese.pdf)
- professeur
[:fontawesome-solid-file-word:{.docx}](assets/python/python_algo%20-%20synthese%20-%20prof.docx)
[:fontawesome-solid-file-pdf:{.pdf}](assets/python/python_algo%20-%20synthese%20-%20prof.pdf)

**TP turtle d'initiation à Python[^1] :** 

- sujet
[:fontawesome-solid-file-word:{.docx}](assets/python/python-structure%20de%20controle%20-%20eleve.docx)
[:fontawesome-solid-file-pdf:{.pdf}](assets/python/python-structure%20de%20controle%20-%20eleve.pdf)
- corrigé
[:fontawesome-solid-file-word:{.docx}](assets/python/python-structure%20de%20controle%20-%20prof.docx)
[:fontawesome-solid-file-pdf:{.pdf}](assets/python/python-structure%20de%20controle%20-%20prof.pdf)

**Correspondance scratch python:**

[:fontawesome-solid-file-word:{.docx}](assets/python/python-Scratch_Python.docx)
[:fontawesome-solid-file-pdf:{.pdf}](assets/python/python-Scratch_Python.pdf)
[:octicons-link-external-16:](tp_scratch_python.md)


**Evaluation**

- cours 
[:fontawesome-solid-file-word:{.docx}](assets/python/python_algo-cours-evaluation.docx)
[:fontawesome-solid-file-pdf:{.pdf}](assets/python/python_algo-cours-evaluation.pdf)

- exercice
[:fontawesome-solid-file-word:{.docx}](assets/python/python_algo-application-evaluation.docx) 
[:fontawesome-solid-file-pdf:{.pdf}](assets/python/python_algo-application-evaluation.pdf)

- correction 
[:simple-python:](assets/python/python-eval.py)


[^1]: idée originale de Maxime Fourny